package com.epam.rd.java.basic.task8;

public class AveLenFlower {
    private int length;
    private final String measure = "cm";

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getMeasure() {
        return measure;
    }
}
