package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flowers;
import com.epam.rd.java.basic.task8.Const;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private String currentElement;
	private Flowers flowers;
	private Flowers.Flower flower;
	private Flowers.Flower.VisualParameters visualParameters;
	private Flowers.Flower.VisualParameters.AveLenFlower aveLenFlower;
	private Flowers.Flower.GrowingTips growingTips;
	private Flowers.Flower.GrowingTips.Watering watering;
	private Flowers.Flower.GrowingTips.Tempreture tempreture;
	private Flowers.Flower.GrowingTips.Lighting lighting;


	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public void parse(boolean validate) throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);
		if (validate) {
			factory.setFeature("http://xml.org/sax/features/validation",true);
			factory.setFeature("http://apache.org/xml/features/validation/schema",true);
		}

		SAXParser parser = factory.newSAXParser();
		parser.parse(xmlFileName,this);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		currentElement = qName;
		switch (currentElement) {
			case Const.FLOWERS: {
				flowers = new Flowers();
				flowers.setSchema(attributes.getValue(Const.SCHEMA_LOCATION));
				flowers.setXmlns_xsi("http://www.w3.org/2001/XMLSchema-instance");
				flowers.setXmlns("http://www.nure.ua");
				break;
			}
			case Const.FLOWER: {
				flower = new Flowers.Flower();
				break;
			}
			case Const.VISUAL_PARAMETERS: {
				visualParameters = new Flowers.Flower.VisualParameters();
				break;
			}
			case Const.LENGTH_FLOWER: {
				aveLenFlower = new  Flowers.Flower.VisualParameters.AveLenFlower();
				aveLenFlower.setMeasure(attributes.getValue(Const.MEASURE));
				break;
			}
			case Const.GROWING_TIPS: {
				growingTips = new Flowers.Flower.GrowingTips();
				break;
			}
			case Const.TEMPERATURE: {
				tempreture = new Flowers.Flower.GrowingTips.Tempreture();
				tempreture.setMeasure(attributes.getValue(Const.MEASURE));
				break;
			}
			case Const.LIGHTING: {
				lighting = new Flowers.Flower.GrowingTips.Lighting();
				lighting.setLightRequiring(attributes.getValue(Const.LIGHT_REQUIRING));
				break;
			}
			case Const.WATERING: {
				watering = new Flowers.Flower.GrowingTips.Watering();
				watering.setMeasure(attributes.getValue(Const.MEASURE));
				break;
			}
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String elementText = new String(ch,start,length).trim();

		switch (currentElement) {
			case Const.NAME: {
				flower.setName(elementText);
				break;
			}
			case Const.SOIL: {
				flower.setSoil(elementText);
				break;
			}
			case Const.ORIGIN: {
				flower.setOrigin(elementText);
				break;
			}
			case Const.STEM_COLOUR: {
				visualParameters.setStemColour(elementText);
				break;
			}
			case Const.LEAF_COLOUR: {
				visualParameters.setLeafColour(elementText);
				break;
			}
			case Const.LENGTH_FLOWER: {
				aveLenFlower.setValue(BigInteger.valueOf(Long.parseLong(elementText)));
				break;
			}
			case Const.TEMPERATURE: {
				tempreture.setValue(BigInteger.valueOf(Long.parseLong(elementText)));
				break;
			}
			case Const.WATERING: {
				watering.setValue(BigInteger.valueOf(Long.parseLong(elementText)));
				break;
			}
			case Const.MULTIPLYING: {
				flower.setMultiplying(elementText);
			}
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (localName) {
			case Const.FLOWER: {
				flowers.getFlower().add(flower);
				flower = null;
				break;
			}
			case Const.VISUAL_PARAMETERS: {
				flower.setVisualParameters(visualParameters);
				visualParameters = null;
				break;
			}
			case Const.GROWING_TIPS: {
				flower.setGrowingTips(growingTips);
				growingTips = null;
				break;
			}
			case Const.LENGTH_FLOWER: {
				visualParameters.setAveLenFlower(aveLenFlower);
				aveLenFlower = null;
				break;
			}
			case Const.TEMPERATURE: {
				growingTips.setTempreture(tempreture);
				tempreture = null;
				break;
			}
			case Const.LIGHTING: {
				growingTips.setLighting(lighting);
				lighting = null;
				break;
			}
			case Const.WATERING: {
				growingTips.setWatering(watering);
				watering = null;
				break;
			}
		}
	}

	public void writeToXML() throws ParserConfigurationException, TransformerException {
		File output = new File("output.sax.xml");
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();

		Element root = doc.createElement(Const.FLOWERS);
		root.setAttribute(Const.XMLNS,flowers.getXmlns());
		root.setAttribute(Const.XMLNS_XSI,flowers.getXmlns_xsi());
		root.setAttribute(Const.SCHEMA_LOCATION,flowers.getSchema());
		doc.appendChild(root);

		for (Flowers.Flower flower: flowers.getFlower()) {

			Element flowerElement = doc.createElement(Const.FLOWER);
			root.appendChild(flowerElement);

			Element nameElement = doc.createElement(Const.NAME);
			nameElement.setTextContent(flower.getName());
			flowerElement.appendChild(nameElement);

			Element soilElement = doc.createElement(Const.SOIL);
			soilElement.setTextContent(flower.getSoil());
			flowerElement.appendChild(soilElement);

			Element originElement = doc.createElement(Const.ORIGIN);
			originElement.setTextContent(flower.getOrigin());
			flowerElement.appendChild(originElement);

			Element visualParametersElement = doc.createElement(Const.VISUAL_PARAMETERS);
			Flowers.Flower.VisualParameters tempVisualParameters = flower.getVisualParameters();

			Element stemColourElement = doc.createElement(Const.STEM_COLOUR);
			stemColourElement.setTextContent(tempVisualParameters.getStemColour());
			visualParametersElement.appendChild(stemColourElement);

			Element leafColourElement = doc.createElement(Const.LEAF_COLOUR);
			leafColourElement.setTextContent(tempVisualParameters.getLeafColour());
			visualParametersElement.appendChild(leafColourElement);

			Element aveLenFlowerElement = doc.createElement(Const.LENGTH_FLOWER);
			aveLenFlowerElement.setAttribute(Const.MEASURE,tempVisualParameters.getAveLenFlower().getMeasure());
			aveLenFlowerElement.setTextContent(tempVisualParameters.getAveLenFlower().getValue().toString());
			visualParametersElement.appendChild(aveLenFlowerElement);

			flowerElement.appendChild(visualParametersElement);

			Element growingTipsElement = doc.createElement(Const.GROWING_TIPS);
			Flowers.Flower.GrowingTips tempGrowingTips = flower.getGrowingTips();

			Element temperatureElement = doc.createElement(Const.TEMPERATURE);
			temperatureElement.setAttribute(Const.MEASURE,tempGrowingTips.getTempreture().getMeasure());
			temperatureElement.setTextContent(tempGrowingTips.getTempreture().getValue().toString());
			growingTipsElement.appendChild(temperatureElement);

			Element lightingElement = doc.createElement(Const.LIGHTING);
			lightingElement.setAttribute(Const.LIGHT_REQUIRING,tempGrowingTips.getLighting().getLightRequiring());
			growingTipsElement.appendChild(lightingElement);

			Element wateringElement = doc.createElement(Const.WATERING);
			wateringElement.setAttribute(Const.MEASURE,tempGrowingTips.getWatering().getMeasure());
			wateringElement.setTextContent(tempGrowingTips.getWatering().getValue().toString());
			growingTipsElement.appendChild(wateringElement);

			flowerElement.appendChild(growingTipsElement);

			Element multiplyingElement = doc.createElement(Const.MULTIPLYING);
			multiplyingElement.setTextContent(flower.getMultiplying());
			flowerElement.appendChild(multiplyingElement);


		}

		StreamResult result = new StreamResult(output);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT,"yes");

		t.transform(new DOMSource(doc),result);
	}

	public Flowers getFlowers() {
		return flowers;
	}
}