package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.AveLenFlower;
import com.epam.rd.java.basic.task8.Const;
import com.epam.rd.java.basic.task8.Flowers;
import com.epam.rd.java.basic.task8.VisualParameters;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private Flowers flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	// PLACE YOUR CODE HERE
	public void parseToDom() throws ParserConfigurationException, IOException, SAXException {
		File input = new File(xmlFileName);
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(input);

		if (flowers == null) flowers = new Flowers();
		List<Flowers.Flower> result = flowers.getFlower();

		Element root = doc.getDocumentElement();
	    flowers.setXmlns(root.getAttribute(Const.XMLNS));
		flowers.setXmlns_xsi(root.getAttribute(Const.XMLNS_XSI));
		flowers.setSchema(root.getAttribute(Const.SCHEMA_LOCATION));
		NodeList flowersNodes = doc.getFirstChild().getChildNodes();

		int length = flowersNodes.getLength();
		for (int i = 0; i < flowersNodes.getLength(); i++) {
			if (flowersNodes.item(i).getNodeType()!= Node.ELEMENT_NODE)
				continue;
			NodeList childFlowersNodes = flowersNodes.item(i).getChildNodes();
			Flowers.Flower flower = new Flowers.Flower();
			result.add(flower);
			for (int j = 0; j < childFlowersNodes.getLength();j++) {
				if (childFlowersNodes.item(j).getNodeType() != Node.ELEMENT_NODE)
					continue;

				switch (childFlowersNodes.item(j).getNodeName()) {
					case Const.NAME : {
						flower.setName(childFlowersNodes.item(j).getTextContent());
						break;
					}
					case Const.SOIL : {
						flower.setSoil(childFlowersNodes.item(j).getTextContent());
						break;
					}
					case Const.ORIGIN : {
						flower.setOrigin(childFlowersNodes.item(j).getTextContent());
						break;
					}
					case Const.VISUAL_PARAMETERS : {
						NodeList visualParametersNodesList = childFlowersNodes.item(j).getChildNodes();
						flower.setVisualParameters(getVisualParameters(visualParametersNodesList));
						break;
					}
					case Const.GROWING_TIPS : {
						NodeList growingTipsNodesList = childFlowersNodes.item(j).getChildNodes();
						flower.setGrowingTips(getGrowingTips(growingTipsNodesList));
						break;
					}
					case Const.MULTIPLYING : {
						flower.setMultiplying(childFlowersNodes.item(j).getTextContent());
						break;
					}
				}
			}

		}
	}

	public void writeToXML() throws ParserConfigurationException, TransformerException {
		File output = new File("output.dom.xml");
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();

		Element root = doc.createElement(Const.FLOWERS);
		root.setAttribute(Const.XMLNS,flowers.getXmlns());
		root.setAttribute(Const.XMLNS_XSI,flowers.getXmlns_xsi());
		root.setAttribute(Const.SCHEMA_LOCATION,flowers.getSchema());
		doc.appendChild(root);

		for (Flowers.Flower flower: flowers.getFlower()) {

			Element flowerElement = doc.createElement(Const.FLOWER);
			root.appendChild(flowerElement);

			Element nameElement = doc.createElement(Const.NAME);
			nameElement.setTextContent(flower.getName());
			flowerElement.appendChild(nameElement);

			Element soilElement = doc.createElement(Const.SOIL);
			soilElement.setTextContent(flower.getSoil());
			flowerElement.appendChild(soilElement);

			Element originElement = doc.createElement(Const.ORIGIN);
			originElement.setTextContent(flower.getOrigin());
			flowerElement.appendChild(originElement);

			Element visualParametersElement = doc.createElement(Const.VISUAL_PARAMETERS);
			Flowers.Flower.VisualParameters tempVisualParameters = flower.getVisualParameters();

			Element stemColourElement = doc.createElement(Const.STEM_COLOUR);
			stemColourElement.setTextContent(tempVisualParameters.getStemColour());
			visualParametersElement.appendChild(stemColourElement);

			Element leafColourElement = doc.createElement(Const.LEAF_COLOUR);
			leafColourElement.setTextContent(tempVisualParameters.getLeafColour());
			visualParametersElement.appendChild(leafColourElement);

			Element aveLenFlowerElement = doc.createElement(Const.LENGTH_FLOWER);
			aveLenFlowerElement.setAttribute(Const.MEASURE,tempVisualParameters.getAveLenFlower().getMeasure());
			aveLenFlowerElement.setTextContent(tempVisualParameters.getAveLenFlower().getValue().toString());
			visualParametersElement.appendChild(aveLenFlowerElement);

			flowerElement.appendChild(visualParametersElement);

			Element growingTipsElement = doc.createElement(Const.GROWING_TIPS);
			Flowers.Flower.GrowingTips tempGrowingTips = flower.getGrowingTips();

			Element temperatureElement = doc.createElement(Const.TEMPERATURE);
			temperatureElement.setAttribute(Const.MEASURE,tempGrowingTips.getTempreture().getMeasure());
			temperatureElement.setTextContent(tempGrowingTips.getTempreture().getValue().toString());
			growingTipsElement.appendChild(temperatureElement);

			Element lightingElement = doc.createElement(Const.LIGHTING);
			lightingElement.setAttribute(Const.LIGHT_REQUIRING,tempGrowingTips.getLighting().getLightRequiring());
			growingTipsElement.appendChild(lightingElement);

			Element wateringElement = doc.createElement(Const.WATERING);
			wateringElement.setAttribute(Const.MEASURE,tempGrowingTips.getWatering().getMeasure());
			wateringElement.setTextContent(tempGrowingTips.getWatering().getValue().toString());
			growingTipsElement.appendChild(wateringElement);

			flowerElement.appendChild(growingTipsElement);

			Element multiplyingElement = doc.createElement(Const.MULTIPLYING);
			multiplyingElement.setTextContent(flower.getMultiplying());
			flowerElement.appendChild(multiplyingElement);


		}

		StreamResult result = new StreamResult(output);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT,"yes");

		t.transform(new DOMSource(doc),result);
	}

	private Flowers.Flower.GrowingTips getGrowingTips(NodeList nodeList) {
		Flowers.Flower.GrowingTips growingTips = new Flowers.Flower.GrowingTips();

		for (int i = 0; i < nodeList.getLength(); i++ ) {
			if (nodeList.item(i).getNodeType() != Node.ELEMENT_NODE) continue;
			switch (nodeList.item(i).getNodeName()) {
				case Const.TEMPERATURE: {
					Flowers.Flower.GrowingTips.Tempreture temp = new Flowers.Flower.GrowingTips.Tempreture();
					temp.setMeasure(nodeList.item(i).getAttributes().item(0).getTextContent());
					temp.setValue(BigInteger.valueOf(Long.parseLong(nodeList.item(i).getTextContent()) ) );
					growingTips.setTempreture(temp);
					break;
				}
				case Const.LIGHTING: {
					Flowers.Flower.GrowingTips.Lighting temp = new Flowers.Flower.GrowingTips.Lighting();
					temp.setLightRequiring(nodeList.item(i).getAttributes().item(0).getTextContent());
					growingTips.setLighting(temp);
					break;
				}
				case Const.WATERING: {
					Flowers.Flower.GrowingTips.Watering temp = new Flowers.Flower.GrowingTips.Watering();
					temp.setMeasure(nodeList.item(i).getAttributes().item(0).getTextContent());
					temp.setValue(BigInteger.valueOf(Long.parseLong(nodeList.item(i).getTextContent())));
					growingTips.setWatering(temp);
					break;
				}
			}
		}
		return growingTips;
	}

	private Flowers.Flower.VisualParameters getVisualParameters(NodeList nodeList) {
		Flowers.Flower.VisualParameters visualParameters = new Flowers.Flower.VisualParameters();

		for (int i = 0; i < nodeList.getLength(); i++ ) {
			if (nodeList.item(i).getNodeType() != Node.ELEMENT_NODE) continue;
			switch (nodeList.item(i).getNodeName()) {
				case Const.STEM_COLOUR: {
					visualParameters.setStemColour(nodeList.item(i).getTextContent());
					break;
				}
				case Const.LEAF_COLOUR: {
					visualParameters.setLeafColour(nodeList.item(i).getTextContent());
					break;
				}
				case Const.LENGTH_FLOWER: {
					Flowers.Flower.VisualParameters.AveLenFlower temp = new Flowers.Flower.VisualParameters.AveLenFlower();
					NamedNodeMap attributes = nodeList.item(i).getAttributes();
					temp.setMeasure(attributes.item(0).getTextContent());
					temp.setValue(BigInteger.valueOf(Long.parseLong(nodeList.item(i).getTextContent()) ) );
					visualParameters.setAveLenFlower(temp);
					break;
				}
			}
		}

		return visualParameters;

	}

	public Flowers getFlowers () {
		return this.flowers;
	}
}
