package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Const;
import com.epam.rd.java.basic.task8.Flowers;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.math.BigInteger;
import java.util.Iterator;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private String currentElement;
	private Flowers flowers;
	private Flowers.Flower flower;
	private Flowers.Flower.VisualParameters visualParameters;
	private Flowers.Flower.VisualParameters.AveLenFlower aveLenFlower;
	private Flowers.Flower.GrowingTips growingTips;
	private Flowers.Flower.GrowingTips.Watering watering;
	private Flowers.Flower.GrowingTips.Tempreture tempreture;
	private Flowers.Flower.GrowingTips.Lighting lighting;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public void parse() throws XMLStreamException {
		currentElement = null;
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE,true);
		XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
			if (event.isCharacters()&&event.asCharacters().isWhiteSpace()) {
				continue;
			}
			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();
				switch (currentElement) {
						case Const.FLOWERS: {
						Iterator<Namespace> namespaceIterator = startElement.getNamespaces();
						Namespace xmlns = namespaceIterator.next();
						Namespace xmlns_xsi = namespaceIterator.next();
						Iterator<Attribute> attributes = startElement.getAttributes();
						Attribute schema = attributes.next();
						flowers = new Flowers();
						flowers.setXmlns(xmlns.getNamespaceURI());
						flowers.setXmlns_xsi(xmlns_xsi.getNamespaceURI());
						flowers.setSchema(schema.getValue());
						break;
					}
					case Const.FLOWER: {
						flower = new Flowers.Flower();
						break;
					}
					case Const.VISUAL_PARAMETERS: {
						visualParameters = new Flowers.Flower.VisualParameters();
						break;
					}
					case Const.LENGTH_FLOWER: {
						Attribute attribute = startElement.getAttributeByName(new QName(Const.MEASURE));
						if (attribute != null){
							aveLenFlower = new Flowers.Flower.VisualParameters.AveLenFlower();
							aveLenFlower.setMeasure(attribute.getValue());
							visualParameters.setAveLenFlower(aveLenFlower);
						}
						break;
					}
					case Const.GROWING_TIPS: {
						growingTips = new Flowers.Flower.GrowingTips();
						break;
					}
					case Const.TEMPERATURE: {
						Attribute attribute = startElement.getAttributeByName(new QName(Const.MEASURE));
						if (attribute!=null) {
							tempreture = new Flowers.Flower.GrowingTips.Tempreture();
							tempreture.setMeasure(attribute.getValue());
							growingTips.setTempreture(tempreture);
						}
						break;
					}
					case Const.LIGHTING: {
						Attribute attribute = startElement.getAttributeByName(new QName(Const.LIGHT_REQUIRING));
						if (attribute != null) {
							lighting = new Flowers.Flower.GrowingTips.Lighting();
							lighting.setLightRequiring(attribute.getValue());
							growingTips.setLighting(lighting);
						}
						break;
					}
					case Const.WATERING: {
						Attribute attribute = startElement.getAttributeByName(new QName(Const.MEASURE));
						if (attribute!=null) {
							watering = new Flowers.Flower.GrowingTips.Watering();
							watering.setMeasure(attribute.getValue());
							growingTips.setWatering(watering);
						}
						break;
					}
				}
			}
			if (event.isCharacters()) {
				Characters characters = event.asCharacters();
				String elementText = characters.getData();
				switch (currentElement) {
					case Const.NAME: {
						flower.setName(elementText);
						break;
					}
					case Const.SOIL: {
						flower.setSoil(elementText);
						break;
					}
					case Const.ORIGIN: {
						flower.setOrigin(elementText);
						break;
					}
					case Const.STEM_COLOUR: {
						visualParameters.setStemColour(elementText);
						break;
					}
					case Const.LEAF_COLOUR: {
						visualParameters.setLeafColour(elementText);
						break;
					}
					case Const.LENGTH_FLOWER: {
						aveLenFlower.setValue(BigInteger.valueOf(Long.parseLong(elementText)));
						break;
					}
					case Const.TEMPERATURE: {
						tempreture.setValue(BigInteger.valueOf(Long.parseLong(elementText)));
						break;
					}
					case Const.WATERING: {
						watering.setValue(BigInteger.valueOf(Long.parseLong(elementText)));
						break;
					}
					case Const.MULTIPLYING: {
						flower.setMultiplying(elementText);
						break;
					}
				}
			}
			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				String local = endElement.getName().getLocalPart();
				switch (local) {
					case Const.FLOWER: {
						flowers.getFlower().add(flower);
						flower = null;
						break;
					}
					case Const.VISUAL_PARAMETERS: {
						flower.setVisualParameters(visualParameters);
						visualParameters = null;
						break;
					}
					case Const.GROWING_TIPS: {
						flower.setGrowingTips(growingTips);
						growingTips = null;
						break;
					}
					case Const.LENGTH_FLOWER: {
						visualParameters.setAveLenFlower(aveLenFlower);
						aveLenFlower = null;
						break;
					}
					case Const.TEMPERATURE: {
						growingTips.setTempreture(tempreture);
						tempreture = null;
						break;
					}
					case Const.LIGHTING: {
						growingTips.setLighting(lighting);
						lighting = null;
						break;
					}
					case Const.WATERING: {
						growingTips.setWatering(watering);
						watering = null;
						break;
					}
				}
			}
		}
		reader.close();
	}

	public void writeToXML() throws ParserConfigurationException, TransformerException {
		File output = new File("output.stax.xml");
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();

		Element root = doc.createElement(Const.FLOWERS);
		root.setAttribute(Const.XMLNS,flowers.getXmlns());
		root.setAttribute(Const.XMLNS_XSI,flowers.getXmlns_xsi());
		root.setAttribute(Const.SCHEMA_LOCATION,flowers.getSchema());
		doc.appendChild(root);

		for (Flowers.Flower flower: flowers.getFlower()) {

			Element flowerElement = doc.createElement(Const.FLOWER);
			root.appendChild(flowerElement);

			Element nameElement = doc.createElement(Const.NAME);
			nameElement.setTextContent(flower.getName());
			flowerElement.appendChild(nameElement);

			Element soilElement = doc.createElement(Const.SOIL);
			soilElement.setTextContent(flower.getSoil());
			flowerElement.appendChild(soilElement);

			Element originElement = doc.createElement(Const.ORIGIN);
			originElement.setTextContent(flower.getOrigin());
			flowerElement.appendChild(originElement);

			Element visualParametersElement = doc.createElement(Const.VISUAL_PARAMETERS);
			Flowers.Flower.VisualParameters tempVisualParameters = flower.getVisualParameters();

			Element stemColourElement = doc.createElement(Const.STEM_COLOUR);
			stemColourElement.setTextContent(tempVisualParameters.getStemColour());
			visualParametersElement.appendChild(stemColourElement);

			Element leafColourElement = doc.createElement(Const.LEAF_COLOUR);
			leafColourElement.setTextContent(tempVisualParameters.getLeafColour());
			visualParametersElement.appendChild(leafColourElement);

			Element aveLenFlowerElement = doc.createElement(Const.LENGTH_FLOWER);
			aveLenFlowerElement.setAttribute(Const.MEASURE,tempVisualParameters.getAveLenFlower().getMeasure());
			aveLenFlowerElement.setTextContent(tempVisualParameters.getAveLenFlower().getValue().toString());
			visualParametersElement.appendChild(aveLenFlowerElement);

			flowerElement.appendChild(visualParametersElement);

			Element growingTipsElement = doc.createElement(Const.GROWING_TIPS);
			Flowers.Flower.GrowingTips tempGrowingTips = flower.getGrowingTips();

			Element temperatureElement = doc.createElement(Const.TEMPERATURE);
			temperatureElement.setAttribute(Const.MEASURE,tempGrowingTips.getTempreture().getMeasure());
			temperatureElement.setTextContent(tempGrowingTips.getTempreture().getValue().toString());
			growingTipsElement.appendChild(temperatureElement);

			Element lightingElement = doc.createElement(Const.LIGHTING);
			lightingElement.setAttribute(Const.LIGHT_REQUIRING,tempGrowingTips.getLighting().getLightRequiring());
			growingTipsElement.appendChild(lightingElement);

			Element wateringElement = doc.createElement(Const.WATERING);
			wateringElement.setAttribute(Const.MEASURE,tempGrowingTips.getWatering().getMeasure());
			wateringElement.setTextContent(tempGrowingTips.getWatering().getValue().toString());
			growingTipsElement.appendChild(wateringElement);

			flowerElement.appendChild(growingTipsElement);

			Element multiplyingElement = doc.createElement(Const.MULTIPLYING);
			multiplyingElement.setTextContent(flower.getMultiplying());
			flowerElement.appendChild(multiplyingElement);


		}

		StreamResult result = new StreamResult(output);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT,"yes");

		t.transform(new DOMSource(doc),result);
	}

	public Flowers getFlowers() {
		return flowers;
	}
}